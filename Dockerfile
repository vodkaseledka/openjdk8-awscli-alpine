FROM openjdk:8-alpine

LABEL maintainer="alexander.smirnoff@gmail.com"

RUN set -eux; \
        apk add python3 ; \
        pip3 --no-cache-dir install -U awscli
